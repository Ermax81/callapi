var axios = require('axios') //module pour faire GET/POST externe
    , https = require('https')
;


// PACKAGES
// axios : https://www.npmjs.com/package/axios MIT License

/**
 * AXIOS: http://zetcode.com/javascript/axios/
 * https://github.com/axios/axios
 */

//API
let api = "https://jsonplaceholder.typicode.com/todos/1";

//Tokens
//let token="xxxxxxxxxx"

let headers = (token) => {
    let str1 = 'Bearer';
    let str2 = str1.concat(' ', token);
    return {
        'Authorization': str2,
        'Content-Type': 'application/json'
    };
};

let agent = new https.Agent({
    rejectUnauthorized: false
});

let config = (method, url, headers, data = {}) => {
    return {
        method: method,
        url: url,
        headers : headers,
        data: data,
        httpsAgent: agent
    };
}

let configTest = ( ) => {
    console.log('API: %s', api);
    return config( 'GET', api, {}, {} );
};

let testGet = async ( ) => {
    try {
        let res = await axios(configTest());
        if (res.status=='200' || res.status=='201' ){
            console.log(res.data);
            return res.data;
        } else {
            console.log(res);
        }
    } catch (error) {
        console.error(error);
    }
};


module.exports = { testGet
};